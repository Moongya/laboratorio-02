package Kata2;

public class Kata {

    public String winner(String[] Steve, String[] Josh) {
        int scoreSteve = 0;
        int scoreJosh = 0;

        String cardRank = "23456789TJQKA";

        for (int i = 0; i < Steve.length; i++) {
            char cardSteve = Steve[i].charAt(0);
            char cardJosh = Josh[i].charAt(0);

            if (cardRank.indexOf(cardSteve) > cardRank.indexOf(cardJosh)) {
                scoreSteve++;
            } else if (cardRank.indexOf(cardJosh) > cardRank.indexOf(cardSteve)) {
                scoreJosh++;
            }
        }

        if (scoreSteve > scoreJosh) {
            return "Steve wins " + scoreSteve + " to " + scoreJosh;
        } else if (scoreJosh > scoreSteve) {
            return "Josh wins " + scoreJosh + " to " + scoreSteve;
        } else {
            return "Tie";
        }
    }
}