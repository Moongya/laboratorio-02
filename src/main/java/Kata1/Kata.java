public class Kata {

    public static String solution(String str) {
        String word = "";
        for (int i = str.length() - 1; i >= 0; i--){
            word += str.charAt(i);
        }
        return word;
    }
}